BAM Index GeneFlow App
======================

Version: 1.10-02

This GeneFlow app wraps SAMTools to generate a BAM index file, BAI.

Inputs
------

1. input: Input BAM File, sorted by coordinate.

Parameters
----------

1. output: Output Directory - The name of the output directory that will contain BAM and BAI files.

